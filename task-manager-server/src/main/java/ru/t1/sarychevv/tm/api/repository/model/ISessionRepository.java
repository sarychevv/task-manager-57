package ru.t1.sarychevv.tm.api.repository.model;

import ru.t1.sarychevv.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}