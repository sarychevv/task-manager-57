package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    IUserDTOService getUserDTOService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IConnectionService getConnectionService();
}
