package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.sarychevv.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId,
                    @Nullable Sort sort) throws Exception;

    @Nullable
    M removeOneById(@Nullable String userId,
                    @Nullable String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable String userId,
                       @Nullable Integer id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    M changeStatusById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable Status status) throws Exception;

    @NotNull
    M changeStatusByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable Status status) throws Exception;

    @NotNull
    M updateById(@Nullable String userId,
                 @Nullable String id,
                 @Nullable String name,
                 @Nullable String description) throws Exception;

    @NotNull
    M updateByIndex(@Nullable String userId,
                    @Nullable Integer index,
                    @Nullable String name,
                    @Nullable String description) throws Exception;
}

