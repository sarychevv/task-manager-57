package ru.t1.sarychevv.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Session;
import ru.t1.sarychevv.tm.model.Task;
import ru.t1.sarychevv.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

@Service
@NoArgsConstructor
public final class ConnectionService implements IConnectionService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Getter
    @NotNull
    @Autowired
    private EntityManager entityManager;

    @Override
    public void close() {
        entityManagerFactory.close();
    }

    @Override
    @NotNull
    @SneakyThrows
    public Liquibase getLiquibase() {
        @NotNull final Connection connection = DriverManager.getConnection(
                propertyService.getDatabaseUrl(),
                propertyService.getDatabaseUsername(),
                propertyService.getDatabasePassword());
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        @NotNull final String fileName = "changelog/changelog-master.xml";
        @NotNull final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        return new Liquibase(fileName, accessor, database);
    }

}

