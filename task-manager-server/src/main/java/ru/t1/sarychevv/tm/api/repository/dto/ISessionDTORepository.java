package ru.t1.sarychevv.tm.api.repository.dto;

import ru.t1.sarychevv.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {
}

