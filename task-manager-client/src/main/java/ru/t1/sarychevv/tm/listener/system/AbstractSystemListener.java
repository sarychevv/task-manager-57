package ru.t1.sarychevv.tm.listener.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.ILoggerService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.listener.AbstractListener;

@Getter
@Setter
@Component
public abstract class AbstractSystemListener extends AbstractListener {
    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

    @Nullable
    @Autowired
    protected AbstractListener[] listeners;

}
