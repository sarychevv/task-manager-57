package ru.t1.sarychevv.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.ILoggerService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.ITokenService;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.listener.AbstractListener;
import ru.t1.sarychevv.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sarychevv.tm.exception.system.CommandNotSupportedException;
import ru.t1.sarychevv.tm.util.SystemUtil;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Getter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.sarychevv.tm.command";

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;
    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;
    @NotNull
    @Getter
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    public void start(@Nullable String[] args) throws Exception {
        processArguments(args);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initFileScanner();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        fileScanner.close();
    }

    private void processArguments(@Nullable final String[] args) throws Exception {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String arg) throws Exception {
        @Nullable final AbstractListener abstractListener = getListenerByArgument(arg);
        if (abstractListener == null) throw new ArgumentNotSupportedException(arg);
        abstractListener.execute(new ConsoleEvent(abstractListener.getName()));
    }

    @Nullable
    private AbstractListener getListenerByArgument(@Nullable final String argument) {
        for (@Nullable final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    @Nullable
    public AbstractListener getListenerByCommand(@Nullable final String command) {
        for (@Nullable final AbstractListener listener : listeners) {
            if (command.equals(listener.getName())) return listener;
        }
        return null;
    }

}
