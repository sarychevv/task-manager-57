package ru.t1.sarychevv.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.task.TaskClearRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;

@Component
public class TaskClearListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    @EventListener(condition = "@TaskClearListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[TASKS CLEAR]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

}
