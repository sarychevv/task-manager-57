package ru.t1.sarychevv.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    @EventListener(condition = "@ProjectCreateListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setDescription(description);
        request.setName(name);
        getProjectEndpoint().createProject(request);
    }

}
