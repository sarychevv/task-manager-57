package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.response.system.ServerVersionResponse;
import ru.t1.sarychevv.tm.event.ConsoleEvent;

@Component
public class ApplicationVersionListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@ApplicationVersionListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(new ApplicationVersionRequest());
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show program version.";
    }

}
