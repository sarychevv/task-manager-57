package ru.t1.sarychevv.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.user.UserRemoveRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class UserRemoveListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getDescription() {
        return "User remove";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    @EventListener(condition = "@UserRemoveListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().removeUser(request);
    }

}
