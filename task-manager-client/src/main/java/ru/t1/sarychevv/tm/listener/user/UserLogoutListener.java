package ru.t1.sarychevv.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;

@Component
public class UserLogoutListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getDescription() {
        return "User logout.";
    }

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    @EventListener(condition = "@UserLogoutListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpoint().logout(request);
    }

}
