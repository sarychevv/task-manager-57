package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.response.system.ServerAboutResponse;
import ru.t1.sarychevv.tm.event.ConsoleEvent;

@Component
public class ApplicationAboutListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@ApplicationAboutListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(new ApplicationAboutRequest());
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show about program.";
    }

}
