package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.listener.AbstractListener;
import ru.t1.sarychevv.tm.dto.request.system.DropSchemeRequest;
import ru.t1.sarychevv.tm.dto.response.system.DropSchemeResponse;

@Component
public class DropSchemeListener extends AbstractListener {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Drop scheme.";
    }

    @Override
    @NotNull
    public String getName() {
        return "drop";
    }

    @Override
    @EventListener(condition = "@DropSchemeListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DROP SCHEME]");
        @NotNull final DropSchemeResponse response = getSystemEndpoint().dropScheme(new DropSchemeRequest(getToken()));
        System.out.println(response);
    }
}
